import './App.css';
import Main from "./views/Main";
import NotFound from "./views/NotFound";
import Login  from "./views/Login";
import RegistroVisitasView  from "./views/RegistroVisita/RegistroVisitaView";
import { BrowserRouter, Route, Switch } from "react-router-dom"


function App() {
  return (
    <div className='App'>
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Login}/>
          <Route exact path='/nuevoregistrovisita' component={Main}/>
          <Route exact path='/registrovisita' component={RegistroVisitasView}/>
          <Route component={NotFound}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
