import React, { Component, Fragment } from 'react'
import { Button, Form, Navbar } from "react-bootstrap";
import Cookies from 'universal-cookie'
import "../css/Huincha.css";
const cookies = new Cookies()


export default class Huincha extends Component {
    constructor() {
        super()
        this.state = {

        }
    }

    CierreSesion = async () => {
        try {
            await cookies.remove("token")
            window.location = window.location.origin
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <Fragment>
                <Navbar bg='dark' variant='dark' className='justify-content-between'>
                    <Navbar.Brand>BITÁCORA</Navbar.Brand>
                    <Form inline>
                        <Button variant='outline-light' onClick={this.CierreSesion}>Cerrar Sesión</Button>
                    </Form>
                </Navbar>
            </Fragment>
        )
    }
}