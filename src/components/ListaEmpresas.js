import React, {Component,Fragment} from 'react'
const axios = require('axios')
const Cookies = require('universal-cookie')
const cookies = new Cookies()
const config_token = require('../config.json')['token-info']


export default class ListaEmpresas extends Component{
    state = {
        empresas:[]

    }
    
    componentDidMount(){
        console.log(config_token)
        const token = cookies.get('token')
        axios.get('http://localhost:3001/empresa/listasimple')//,{headers: {config_token.name: 'foobar'}})
        .then(success =>{
            return success.data.json()
            
        })
        .then(data =>{
            this.setState({empresas:data})
        })
        .catch(err => {
            console.log(err);
        })
    }

    render(){
        return(
            <Fragment>
                <select>
                    {this.state.empresas.map(empresa => {<option id={empresa.id_empresa}>{empresa.razon_social}</option>})}
                </select>
            </Fragment>
        )
    }
}