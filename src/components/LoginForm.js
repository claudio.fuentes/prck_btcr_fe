import React, {Component,Fragment} from 'react'
import {Form,Card,Alert} from 'react-bootstrap'
import { FaUserCircle } from 'react-icons/fa'
import { IconContext } from "react-icons"
import {ValidarUsuario} from '../models/Usuario'
import Cookies from 'universal-cookie'
import md5 from "md5";
import "../css/LoginForm.css";

const cookies = new Cookies()

export default class LoginForm extends Component{
    constructor(){
        super()
        this.state = {
            usuario:"",
            contrasenna:"",
            passErroneo:false,
            recuerdame:true
        }
    }
    componentDidMount(){
        
        if (cookies.get('token') != null) {    
            window.location = './registrovisita'
        }else{
            //TODO revisar porque no entra al if
            if (cookies.get('user')!= null) {
                const user = cookies.get('user')
                const pass = cookies.get('pass')
                this.setState({
                    usuario:user,
                    contrasenna:pass
                })
            }
        }
    }

    handleInput= e =>{
        const {name,value} = e.target
        this.setState({
            [name]:value
        })
    }

    handleCheck= e => {
        this.setState({
            recuerdame:e.target.checked
        })
    }

    handleSubmit = e =>{
        e.preventDefault()
        let data = {usuario:this.state.usuario,contrasenna:md5(this.state.contrasenna)}
        this.ValidarRecuerdame()
        console.time("login")
        ValidarUsuario(data).then(dt => {
            if (dt.code === -1) {
                this.setState({passErroneo:true})
            }
            else
            {
                cookies.set('token',dt.token,{path:'/'})
                this.setState({passErroneo:false})
                window.location.href='/registrovisita'
            }
        })
        console.timeEnd("login")
    }

    ValidarRecuerdame = ()=>{
        const {usuario,contrasenna,recuerdame} = this.state
        if (recuerdame === true) {
            cookies.set('user',usuario,{path:'/'})
            cookies.set('pass',contrasenna,{path:'/'})
        }else{
            cookies.remove('user')
            cookies.remove('pass')
        }
    }


    render(){
        
        return(
            <Fragment>
                <Card border='black' className='p-5'>
                    <Form onSubmit={this.handleSubmit}>
                        <IconContext.Provider value={{size:"10em",color:"#483D8B",title:"imagen login"}}>
                            <FaUserCircle/>
                        </IconContext.Provider>
                        <br/>
                        <Form.Label><h1>Inicio de Sesión</h1></Form.Label>
                        <Form.Group className='mt-3'>
                            <Form.Label htmlFor='user'>usuario</Form.Label>
                            <Form.Control type='text' as='input' htmlSize='150' id='user' autoFocus name='usuario' onChange={this.handleInput} value={this.state.usuario}/>
                        </Form.Group>
                        <Form.Group className='mt-3'>
                            <Form.Label htmlFor='pass'>contraseña</Form.Label>
                            <Form.Control as='input' type='password' htmlSize='15' id='pass'name='contrasenna' onChange={this.handleInput} value={this.state.contrasenna}/>
                        </Form.Group>
                        <Form.Group className='mt-5'>
                            <Form.Control className='btn btn-primary block mt-4' as='input' type='submit' value='Acceder'/>
                            <Form.Check type='checkbox' label='recuerdame' checked={this.state.recuerdame} name='recuerdame' onChange={this.handleCheck}/>
                        </Form.Group>
                    </Form>
                </Card>
                <Alert variant='danger' show={this.state.passErroneo}>Usuario o contraseña inválido</Alert>
            </Fragment>
        )
    }
}