import React, { Component, Fragment } from 'react'
import { CargaEmpresa } from "../models/Empresa";
import { CargaSucursal } from "../models/Sucursal";
import { CargaPersonal } from "../models/Personal";
import { NuevoRegistro } from "../models/RegistroVisita";

import { informacionAdicional } from "../config.json"

export default class RegistroVisitaForm extends Component {

    constructor() {
        super()
        this.state = {
            empresas: [],
            sucursales: [],
            anfitriones: [],
            responsables: [],
            asistentes: [],
            idEmpresa: 1,
            idSucursal: 1,
            idAnfitrion: 1,
            idResponsable: 1,
            idAsistente: 1,
            horaInicio: '',
            horaTermino: '',
            especificacion: '',
            solicitud: '',
            compra: '',
            observacion: ''
        }
        this.handleEmpresaChange = this.handleEmpresaChange.bind(this)
        this.handleInput = this.handleInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleEmpresaChange(e) {
        this.setState({ idEmpresa: e.target.value })
        CargaSucursal(this.state.idEmpresa).then(success => {
            this.setState({ sucursales: success })
            CargaPersonal(this.state.idEmpresa).then(success => {
                this.setState({ anfitriones: success })
            })
        })
    }

    handleInput(e) {
        const { value, name } = e.target
        this.setState({
            [name]: value
        })
    }
    handleSubmit() {
        const data = {
            horaInicio: this.state.horaInicio,
            horaTermino: this.state.horaTermino,
            especificaciones: this.state.especificacion,
            solicitudes: this.state.solicitud,
            compras: this.state.compra,
            observaciones: this.state.observacion,
            responsable: parseInt(this.state.idResponsable),
            asistente: this.state.idAsistente,
            anfitrion: this.state.idAnfitrion,
            sucursal: this.state.idSucursal
        }
        NuevoRegistro(data).then(success => console.log(success))

    }

    componentDidMount() {
        CargaEmpresa()
        .then(success => {
            this.setState({ empresas: success,idEmpresa:success[0].id_empresa })
        })
        .then(() => {
            CargaSucursal(this.state.idEmpresa)
            .then(success => {
                this.setState({ sucursales: success })
            })
            .then(() => {
                CargaPersonal(this.state.idEmpresa).then(success => {
                    this.setState({ anfitriones: success })
                    CargaPersonal(informacionAdicional.idEmpresaOrigen).then(success => {
                        this.setState({ responsables: success, asistentes: success })
                    })
                })
            })
        }
        )

    }

    render() {
        let selectEmpresas = this.state.empresas.map(empresa =>
            (<option key={empresa.id_empresa} value={empresa.id_empresa}>{empresa.razonSocial}</option>)
        )
        let selectSucursales = this.state.sucursales.map(sucursal =>
            <option key={sucursal.id_sucursal} value={sucursal.id_sucursal} >{sucursal.nombre}</option>
        )
        let selectAnfitriones = this.state.anfitriones.map(anfitrion =>
            <option key={anfitrion.id_personal} value={anfitrion.id_personal} name='idAnfitrion'>{anfitrion.nombre}</option>
        )
        let selectResponsables = this.state.responsables.map(responsable =>
            <option key={responsable.id_personal} value={responsable.id_personal}>{responsable.nombre}</option>
        )
        let selectAsistente = this.state.asistentes.map(asistente =>
            <option key={asistente.id_personal} value={asistente.id_personal}>{asistente.nombre}</option>
        )
        return (
            <Fragment>
                <div className='card'>
                    <div className='card-header'>Nuevo registro de visita</div>
                    <div className='card-body'>
                        <form className='row' onSubmit={this.handleSubmit}>
                            <div className='mt-2 col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                <label className='form-label' htmlFor="empresaId">Empresa</label>
                                <select className='form-select' id='idEmpresa' onChange={this.handleEmpresaChange} name='idEmpresa'>
                                    {
                                        selectEmpresas
                                    }
                                </select>
                            </div>
                            <div className='mt-2 col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                <label className='form-label' htmlFor='idSucursal'>Sucursal</label>
                                <select id='idSucursal' className='form-select' required name='idSucursal' onChange={this.handleInput}>
                                    {
                                        selectSucursales
                                    }
                                </select>
                            </div>
                            <div className='mt-2 col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                <label className='form-label' htmlFor='idAnfitrion'>Anfitrión</label>
                                <select id='idAnfitrion' className='form-select' onChange={this.handleInput} name='idAnfitrion'>
                                    {
                                        selectAnfitriones
                                    }
                                </select>
                            </div>
                            <div className='col-lg-4 col-md-6 col-sm-12 col-xs-12 mt-2'>
                                <label className='form-label' htmlFor='idResponsable' required>responsable</label>
                                <select className='form-select' id='idResponsable' name='idResponsable' onChange={this.handleInput}>
                                    {
                                        selectResponsables
                                    }
                                </select>
                            </div>
                            <div className='col-lg-4 col-md-6 col-sm-12 col-xs-12 mt-2'>
                                <label className='form-label' htmlFor='idAsistente'>asistente</label>
                                <select className='form-select' id='idAsistente' name='idAsistente' onChange={this.handleInput}>
                                    {
                                        selectAsistente
                                    }
                                </select>
                            </div>
                            <div className='col-lg-2 col-md-3 col-sm-6 col-xs-6 mt-2'>
                                <label className='form-label' htmlFor='horaInicio'>Hora de Inicio</label>
                                <input type='time' className='form-control' id='horaInicio' name='horaInicio' onChange={this.handleInput}></input>
                            </div>
                            <div className='col-lg-2 col-md-3 col-sm-6 col-xs-6 mt-2'>
                                <label className='form-label' htmlFor='horaTermino'>Hora de Termino</label>
                                <input type='time' className='form-control' id='horaTermino' name='horaTermino' onChange={this.handleInput}></input>
                            </div>
                            <div className='card mt-3'>
                                <div className='card-title'>Detalle de la visita</div>
                                <div className='card-body'>
                                    <div className='mt-2 col-12'>
                                        <label className='form-label' htmlFor='especificacion'>especificaciones</label>
                                        <textarea className='form-control' id='especificacion' name='especificacion' onChange={this.handleInput}></textarea>
                                        <div id="especificacionHelp" className="form-text">0/5000</div>
                                    </div>
                                    <div className='mt-2 col-12'>
                                        <label className='form-label' htmlFor='solicitud'>solicitudes</label>
                                        <textarea className='form-control' id='solicitud' name='solicitud' onChange={this.handleInput}></textarea>
                                        <div id="solicitudHelp" className="form-text">0/5000</div>
                                    </div>
                                    <div className='mt-2 col-12'>
                                        <label className='form-label' htmlFor='compra'>compras</label>
                                        <textarea className='form-control' id='compra' name='compra' onChange={this.handleInput}></textarea>
                                        <div id="compraHelp" className="form-text">0/5000</div>
                                    </div>
                                    <div className='mt-2 col-12'>
                                        <label className='form-label' htmlFor='observacion'>observaciones</label>
                                        <textarea className='form-control' id='observacion' name='observacion' onChange={this.handleInput}></textarea>
                                        <div id="observacionHelp" className="form-text">0/5000</div>
                                    </div>
                                </div>
                                <div>
                                    <input type='submit' className='btn btn-success btn-lg' value='Guardar'></input>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </Fragment>
        )
    }
}