import React, { Component, Fragment } from 'react'
import { Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap'


export default class RegistroVisitaUnq extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id_rv: 0,
            fecha: "",
            horaInicio: "",
            horaTermino: "",
            especificaciones: "",
            solicitudes: "",
            compras: "",
            observaciones: "",
            responsable: "",
            asistente: "",
            anfitrion: ""
        }
    }
    componentDidMount() {
        this.setState({
            id_rv: this.props.registro.id_rv,
            fecha: this.props.registro.fecha,
            horaInicio: this.props.registro.horaInicio,
            horaTermino: this.props.registro.horaTermino,
            especificaciones: this.props.registro.especificaciones,
            solicitudes: this.props.registro.solicitudes,
            compras: this.props.registro.compras,
            observaciones: this.props.registro.observaciones,
            responsable: this.props.registro.responsable,
            asistente: this.props.registro.asistente,
            anfitrion: this.props.registro.anfitrion
        })
    }

    render() {
        return (
            <Fragment>
                <div className='container'>
                    <div className='row'>
                        <div className='col'>
                            <Form className='row'>
                                <FormGroup className='col-lg-4 col-md-12 col-sm-12 col-xs-12' >
                                    <FormLabel htmlFor='txtFecha'>Fecha</FormLabel>
                                    <FormControl id='txtFecha' value={this.state.fecha.substr(0, 10)} readOnly />
                                </FormGroup>
                                <FormGroup className='col-lg-4 col-md-6 col-sm-12 col-xs-12' >
                                    <FormLabel htmlFor='txtHoraIn'>Hora inicio</FormLabel>
                                    <FormControl id='txtHoraIn' value={this.state.horaInicio} readOnly />
                                </FormGroup>
                                <FormGroup className='col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                    <FormLabel htmlFor='txtHoraFn'>Hora Termino</FormLabel>
                                    <FormControl id='txtHoraFn' value={this.state.horaTermino} readOnly/>
                                </FormGroup>
                                <FormGroup className='col-lg-4 col-md-12 col-sm-12 col-xs-12'>
                                    <FormLabel htmlFor='txtResponsable'>Responsable</FormLabel>
                                    <FormControl id='txtResponsable' value={this.state.responsable} readOnly/>
                                </FormGroup>
                                <FormGroup className='col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                    <FormLabel htmlFor='txtAnfitrion'>Anfitrion</FormLabel>
                                    <FormControl id='txtAnfitrion' value={this.state.anfitrion} readOnly/>
                                </FormGroup>
                                <FormGroup className='col-lg-4 col-md-6 col-sm-12 col-xs-12'>
                                    <FormLabel htmlFor='txtAsistente'>Asistente</FormLabel>
                                    <FormControl id='txtAsistente' value={this.state.asistente} readOnly/>
                                </FormGroup>
                                <FormGroup className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                    <FormLabel htmlFor='txtEspecificaciones'>Especificaciones</FormLabel>
                                    <FormControl id='txtEspecificaciones' value={this.state.especificaciones} readOnly/>
                                </FormGroup>
                                <FormGroup className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                    <FormLabel htmlFor='txtSolicitudes'>Solicitudes</FormLabel>
                                    <FormControl id='txtSolicitudes' value={this.state.solicitudes} readOnly/>
                                </FormGroup>
                                <FormGroup className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                    <FormLabel htmlFor='txtCompras'>Compras</FormLabel>
                                    <FormControl id='txtCompras' value={this.state.compras} readOnly/>
                                </FormGroup>
                                <FormGroup className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                    <FormLabel htmlFor='txtObservaciones'>Observaciones</FormLabel>
                                    <FormControl id='txtObservaciones' value={this.state.observaciones} readOnly/>
                                </FormGroup>
                            </Form>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}