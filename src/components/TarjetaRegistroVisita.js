import React, { Component } from 'react'
import { Fragment } from 'react';
import { Card, Button, Modal, ModalBody, ModalFooter } from 'react-bootstrap'
import ModalHeader from 'react-bootstrap/esm/ModalHeader'
import { GoEye } from "react-icons/go"
import { GrClose } from "react-icons/gr"
import { IconContext} from "react-icons"
import RegistroVisitaUnq from './RegistroVisitaUnq'
import {UniqueShow} from '../models/RegistroVisita'

export default class TarjetaRegistroVisita extends Component {
    constructor(props) {
        super(props)
        this.state = {
            registro: this.props.registro,
            registroCompleto:{},
            showModal: false
        }
    }
    componentDidMount() {
        UniqueShow(this.state.registro.id_rv).then((reg)=>{
            this.setState({registroCompleto:reg})
        })
    }
    OnClickShow = () => {
        this.setState({ showModal: true })
    }
    HandleCloseModal = ()=>{
        this.setState({showModal:false})
    }
    render() {

        return (
            <Fragment>
                <Modal show={this.state.showModal} onHide={this.HandleCloseModal}size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
                    <ModalHeader>
                            <div className='col-11'><h3>{`${this.state.registro.empresa} | ${this.state.registro.sucursal}`}</h3></div>
                            <div className='col-1'>
                                <Button variant='outline-dark' onClick={this.HandleCloseModal} block={true}>
                                    <IconContext.Provider value={{color:'#f00',className:'danger'}}>
                                        <GrClose/>
                                    </IconContext.Provider>
                                </Button>
                            </div>
                        
                    </ModalHeader>
                    <ModalBody>
                        <RegistroVisitaUnq registro={this.state.registroCompleto}></RegistroVisitaUnq>
                    </ModalBody>
                    <ModalFooter>
                        <div className='row'>
                            <div className='col-2 col-offset-5'>
                                <Button variant='secondary' onClick={this.HandleCloseModal}>cerrar</Button>
                            </div>
                        </div>
                    </ModalFooter>
                </Modal>
                <Card className='col-lg-3 col-md-3 col-sm-6 col-xs-12 m-3' variant='light' border='dark'>
                    <Card.Body>
                        <Card.Title>{`${this.state.registro.empresa} | ${this.state.registro.sucursal}`}</Card.Title>
                        <Card.Subtitle>{this.state.registro.anfitrion}</Card.Subtitle>
                        <Card.Text>{this.state.registro.responsable}</Card.Text>
                        <Card.Text>
                            {this.state.registro.fecha.substring(0, 10)}
                        </Card.Text>
                    </Card.Body>
                    <Card.Footer>
                        <Button variant='primary' onClick={this.OnClickShow}><GoEye /> Ver</Button>
                    </Card.Footer>
                </Card>
            </Fragment>

        )
    }
}

