import Cookies from "universal-cookie";
const config = require("../config.json")
const axios = require('axios')
const config_token = require('../config.json')['token-info']


const cookies = new Cookies()
let url = axios.create({
    baseURL:config.apiConection.url,
    headers:
        {
            [config_token.name]: `${config_token.prefix} ${cookies.get('token')}`
        }
})

async function CargaEmpresa(){
    const data = await url.get('/empresa/listasimple')
    return data.data
}
export  {
    CargaEmpresa
}