import Cookies from "universal-cookie";
const config = require("../config.json")
const axios = require('axios')
const config_token = require('../config.json')['token-info']

const cookies = new Cookies()
let url = axios.create({
    baseURL:config.apiConection.url,
    headers:
        {
            [config_token.name]: `${config_token.prefix} ${cookies.get('token')}`
        }
})

async function NuevoRegistro(data) {
    const connection = await url.post('/registrovisita/nuevo',data)
    return connection.data
}

async function ListaCard() {
    const connection = await url.get('/registrovisita/listacard')
    
    return connection.data
}

async function UniqueShow(id){
    const connection = await url.get('/registrovisita/busqueda',{params:{id}})
    return connection.data
}

export {
    NuevoRegistro,
    ListaCard,
    UniqueShow
}