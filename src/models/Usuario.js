const config = require("../config.json")
const axios = require('axios')

let url = axios.create({
    baseURL: config.apiConection.url
})

async function ValidarUsuario(data) {
    const connection = await url.post('/usuario/validarUsuario',data)
    
    return connection.data
}
export {
    ValidarUsuario
}