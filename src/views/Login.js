import React, {Component,Fragment} from 'react'
import  LoginForm from "../components/LoginForm";


export default class Login extends Component{
    render(){
        return(
            <Fragment>
                <div className='container'>
                    <div className='row mt-5'>
                        <div className='col-lg-4 col-md-6 col-sm-8 col-xs-10 offset-lg-4 offset-md-3 offset-sm-2 offset-xs-1'>
                            <LoginForm/>
                        </div>
                    </div>
                </div>
                
            </Fragment>
        )
    }
}