import React, { Component, Fragment } from 'react'
import RegistroVisitaForm from "../components/RegistroVisitaForm";
import Huincha from "../components/Huincha";
import Cookies from 'universal-cookie'

const cookies = new Cookies()



export default class Main extends Component {
    componentDidMount(){
        const token = cookies.get('token')
        if(token == null)
        window.location.href = '/'
    }
    render() {
        return (
            <Fragment>
                <header>
                    <Huincha/>
                </header>
                <div className='container mt-5'>
                    <RegistroVisitaForm />
                </div>

            </Fragment>
        )
    }
}