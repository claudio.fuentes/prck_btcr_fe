import React, {Component,Fragment} from 'react'
import nf from "../images/error404.jpeg";
import Badge from 'react-bootstrap/Badge'



export default class NotFound extends Component{
    render(){
        return(
            <Fragment>
                <h1> Página no encontrada <Badge pill variant="danger">404</Badge></h1>
                <img src={nf} alt='dos pasteles no encontrados'></img>
            </Fragment>
        )
    }
}