import React, {Component,Fragment} from 'react'
import Huincha from "../../components/Huincha";
import { Button, CardDeck } from "react-bootstrap";
import TarjetaRegistroVisita from "../../components/TarjetaRegistroVisita";
import { ListaCard } from "../../models/RegistroVisita";
import Cookie  from "universal-cookie"
const cookies = new Cookie()


export default class RegistroVisitaView extends Component{
    constructor(){
        super()
        this.state = {
            data:[]
        }
    }
    componentDidMount(){
        const token = cookies.get('token')
        if(token == null)
        window.location.href = '/'
        ListaCard().then(success => {
            this.setState({data:success})
        })
    }
    AgregarRegistro=()=>{
        window.location.href='./nuevoregistrovisita'
    }
    render(){
        const CargarCards = this.state.data.map(registro => (
            <TarjetaRegistroVisita key={registro.id_rv} registro={registro}/>
        ))

        return(
            <Fragment>
                <Huincha></Huincha>
                <div className='container'>
                <div className='row mt-3 mb-1 justify-content-end'>
                    <div className='col-2'>
                        <Button variant='success' size='lg' onClick={this.AgregarRegistro}>Agregar</Button>
                    </div>
                </div>
                <CardDeck className='row mt-4'>
                    {CargarCards}
                </CardDeck>
                </div>
            </Fragment>
        )
    }
}